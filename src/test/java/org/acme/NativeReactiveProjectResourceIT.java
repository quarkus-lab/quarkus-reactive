package org.acme;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeReactiveProjectResourceIT extends ReactiveProjectResourceTest {

    // Execute the same tests but in native mode.
}