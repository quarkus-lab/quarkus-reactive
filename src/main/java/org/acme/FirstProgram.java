package org.acme;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.subscription.Cancellable;

import java.time.Duration;
import java.util.Arrays;

public class FirstProgram {
    public static void main(String[] args) {
//        Uni.createFrom().item("hello")
//                .onItem().transform(it -> it + " mutiny")
//                .onItem().transform(String::toUpperCase)
//                .subscribe().with(it -> System.out.println(">> " + it));

//        Uni<String> uni = Uni.createFrom().item(1)
//                .onItem().transform(i -> "hello-" + i)
//                .onItem().delayIt().by(Duration.ofMillis(100));
//
//        Uni<Integer> failed = Uni.createFrom().failure(new Exception("boom"));
//
//        uni.subscribe().with(
//                it -> System.out.println(it),
//                failure -> System.out.println("Failed with " + failure));
//
//        Cancellable cancellable2 = failed.subscribe().with(
//                it -> System.out.println(it),
//                failure -> System.out.println("Failed with " + failure));

//        Multi.createFrom().items(1, 2, 3, 4, 5)
//                .onItem().transform(it -> it * it)
//                .select().first(3)
//                .onFailure().recoverWithItem(0)
//                .subscribe().with(
//                        it -> System.out.println(it),
//                        f -> System.out.println("Failed with " + f),
//                        () -> System.out.println("Completed")
//        );
//
//        Multi.createFrom().iterable(Arrays.asList(1, 2, 3, 4, 5))
//                .subscribe().with(System.out::println);
//
//        Multi.createFrom().emitter(em -> {
//            em.emit(1);
//            em.emit(2);
//            em.emit(3);
//            em.complete();
//        }).subscribe().with(System.out::println);

//        Multi.createFrom().ticks().every(Duration.ofSeconds(1))
//                .subscribe().with(System.out::println);

        Multi.createFrom().range(1, 10)
                .onItem().transform(it -> "number: " + it * it)
                .subscribe().with(System.out::println);

    }
}
