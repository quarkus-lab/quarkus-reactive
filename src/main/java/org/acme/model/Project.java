package org.acme.model;


import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "Project")
@ToString
public class Project extends PanacheEntityBase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String name;
    public String description;
    public String status;
    @Column(name = "create_time")
    public LocalDateTime createTime;
    @Column(name = "update_time")
    public LocalDateTime updateTime;


}
