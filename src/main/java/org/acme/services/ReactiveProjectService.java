package org.acme.services;

import io.quarkus.hibernate.reactive.panache.Panache;
import io.quarkus.hibernate.reactive.panache.PanacheEntityBase;
import io.smallrye.mutiny.Uni;
import lombok.extern.slf4j.Slf4j;
import org.acme.model.Project;
import org.acme.resources.dto.AddProjectReqDto;
import org.acme.resources.dto.ProjectRespDto;
import org.acme.resources.dto.UpdateProjectReqDto;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import javax.ws.rs.Produces;
import java.time.LocalDateTime;
import java.util.List;

@ApplicationScoped
@Slf4j
public class ReactiveProjectService {

    public Uni<List<PanacheEntityBase>> getAllProjects() {
        return Project.<Project>listAll()
                .onItem().transform(it -> {
                    ModelMapper modelMapper = new ModelMapper();
                    return modelMapper.map(it, new TypeToken<List<ProjectRespDto>>() {
                    }.getType());
                });
    }

    public Uni<ProjectRespDto> getProjectById(Integer id) {
        return Project.<Project>findById(id).onItem().ifNotNull().transform(it -> {
            ModelMapper modelMapper = new ModelMapper();
            return modelMapper.map(it, new TypeToken<ProjectRespDto>() {
            }.getType());
        });
    }

    @Transactional(rollbackOn = Exception.class)
    public Uni<Project> addProject(AddProjectReqDto request) {
        LocalDateTime now = LocalDateTime.now();

        Project project = new Project();
        project.name = request.getName();
        project.description = request.getDescription();
        project.status = request.getStatus();
        project.createTime = now;
        project.updateTime = now;

        log.info(project.toString());

        return Panache.withTransaction(project::persist);
    }

    @Transactional(rollbackOn = Exception.class)
    public Uni<Project> updateProject(UpdateProjectReqDto request) {

        LocalDateTime now = LocalDateTime.now();
        Integer id = request.getId();
        String name = request.getName();
        String description = request.getDescription();
        String status = request.getStatus();

        return Panache.withTransaction(() -> Project.<Project>findById(id).onItem().ifNotNull().invoke(it -> {
            it.name = name;
            it.description = description;
            it.status = status;
            it.createTime = now;
            it.updateTime = now;
        }));
    }
}
