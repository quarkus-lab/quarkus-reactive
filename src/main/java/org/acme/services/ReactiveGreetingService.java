package org.acme.services;

import io.smallrye.mutiny.Uni;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import java.util.concurrent.ExecutionException;

@Slf4j
@ApplicationScoped
public class ReactiveGreetingService {

    public Uni<String> greeting(String name) throws ExecutionException, InterruptedException {
        log.info("greeting start");
        String uni = Uni.createFrom().item(name)
                .onItem().transform(it -> "hello " + it)
                .subscribeAsCompletionStage().get();
        log.info(uni);

        return Uni.createFrom().item(name)
                .onItem().transform(it -> "hello " + it);
    }

}
