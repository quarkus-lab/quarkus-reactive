package org.acme.resources;

import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import org.acme.resources.dto.AddProjectReqDto;
import org.acme.resources.dto.UpdateProjectReqDto;
import org.acme.services.ReactiveProjectService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.concurrent.CompletableFuture;

@Path("/projects")
@ApplicationScoped
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ReactiveProjectResource {

    @Inject
    ReactiveProjectService reactiveProjectService;

    @GET
    public CompletableFuture<Response> getAllProjects() {
        return reactiveProjectService.getAllProjects()
                .onItem().transform(it -> Response.ok(it).build())
                .subscribe().asCompletionStage();
    }

    @GET
    @Path("/{id}")
    public Uni<Response> getProjectById(@PathParam("id") Integer id) {
        return reactiveProjectService.getProjectById(id)
                .onItem().ifNotNull().transform(it -> Response.ok(it).build())
                .onItem().ifNull().continueWith(Response.noContent().build());
    }

    @POST
    public Uni<Response> addProject(AddProjectReqDto request) {
        return reactiveProjectService.addProject(request)
                .onItem().transform(it -> Response.ok(it).status(Response.Status.CREATED).build());
    }

    @PUT
    public Uni<Response> updateProject(UpdateProjectReqDto request) {
        return reactiveProjectService.updateProject(request)
                .onItem().ifNotNull().transform(it -> Response.ok(it).build())
                .onItem().ifNull().continueWith(Response.ok().status(Status.NOT_FOUND)::build);
    }
}