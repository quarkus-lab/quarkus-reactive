package org.acme.resources.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProjectRespDto {
    @JsonProperty("ID")
    private Integer id;
    @JsonProperty("NAME")
    private String name;
    @JsonProperty("DESCRIPTION")
    private String description;
    @JsonProperty("STATUS")
    private String status;
    @JsonProperty("CREATE_TIME")
    private String createTime;
    @JsonProperty("UPDATE_TIME")
    private String updateTime;
}
