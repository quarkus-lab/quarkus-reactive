package org.acme.resources.dto;

import lombok.Data;

@Data
public class AddProjectReqDto {

    private String name;
    private String description;
    private String status;
}
