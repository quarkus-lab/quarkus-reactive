package org.acme;

import io.quarkus.runtime.Quarkus;
import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import org.jboss.logging.Logger;

import javax.inject.Inject;

@QuarkusMain
public class ExampleMain implements QuarkusApplication {

    @Inject
    Logger log;

    @Override
    public int run(String... args) throws Exception {
        log.info("quarkus is running!");
        Quarkus.waitForExit();
        return 0;
    }

    public static void main(String... args) {
        Quarkus.run(ExampleMain.class, args);
    }
}
